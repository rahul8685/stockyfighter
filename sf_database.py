import pickle
from enum import Enum

from peewee import Model, TextField, BlobField, DoesNotExist, IntegerField, DateTimeField, fn, CompositeKey, \
    BooleanField
from playhouse.sqlite_ext import SqliteExtDatabase
from datetime import datetime, timedelta

from utils import get_logger

db = SqliteExtDatabase('sf_database.db',
                       pragmas=[('cache_size', '100000'),
                                ('page_size', '8192'),
                                ('journal_mode', 'WAL'),
                                ('synchronous', 'OFF'),
                                ('temp_store', 'MEMORY'),
                                ('mmap_size', 1024 * 1024 * 32),
                                ])
mdb = SqliteExtDatabase(':memory:')
logger = get_logger(__name__)


class BaseModel(Model):
    class Meta:
        database = db


class Direction(Enum):
    buy = 'buy'
    sell = 'sell'


class KeyValue(BaseModel):
    key = TextField(unique=True)
    value = BlobField()

    @staticmethod
    def get_key(k, default=None):
        try:
            v = KeyValue.get(KeyValue.key == k).value
            return pickle.loads(v, encoding='UTF-8')
        except DoesNotExist:
            if default:
                return default
            raise

    @staticmethod
    def set_key(k, v):
        vp = pickle.dumps(v)
        try:
            kv = KeyValue.get(key=k)
            kv.value = vp
            kv.save()
        except DoesNotExist:
            KeyValue.create(key=k, value=vp)


if not KeyValue.table_exists():
    KeyValue.create_table()


class Venue(BaseModel):
    symbol = TextField(index=True)
    venue = TextField(index=True)
    bid = IntegerField(index=True, null=True)
    ask = IntegerField(index=True, null=True)
    bid_size = IntegerField(index=True, null=True)
    ask_size = IntegerField(index=True, null=True)
    bid_depth = IntegerField(index=True, null=True)
    ask_depth = IntegerField(index=True, null=True)
    quote_time = DateTimeField(index=True)
    market_maker = BooleanField(index=True)


if not Venue.table_exists():
    Venue.create_table()


class VenueUtil:
    def __init__(self, symbol, venue):
        self.symbol = symbol
        self.venue = venue

    def get(self):
        return Venue.filter(Venue.symbol == self.symbol,
                            Venue.venue == self.venue)

    @staticmethod
    def record(symbol, venue, bid, ask, bid_size, ask_size, bid_depth, ask_depth, quote_time):
        logger.info('Record Quote: {}:{}->b:{}, a:{}, bs:{}, as:{}, bd:{}, ad:{}, t:{}'.format(
                symbol,
                venue,
                bid,
                ask,
                bid_size,
                ask_size,
                bid_depth,
                ask_depth,
                quote_time
        ))
        Venue.create(
                symbol=symbol,
                venue=venue,
                bid=bid,
                ask=ask,
                bid_size=bid_size,
                ask_size=ask_size,
                bid_depth=bid_depth,
                ask_depth=ask_depth,
                quote_time=quote_time,
                market_maker=bid and ask
        )

    def count_till(self, time_delta=timedelta(seconds=10)):
        delta_time = datetime.utcnow() - time_delta
        return self.get().filter(Venue.quote_time > delta_time).count()

    def max_min_bid_till(self, time_delta=timedelta(seconds=10)):
        delta_time = datetime.utcnow() - time_delta
        ma, mi = self.get().filter(Venue.bid != 0,
                                   Venue.quote_time > delta_time
                                   ).select(fn.max(Venue.bid), fn.min(Venue.bid)
                                            ).scalar(as_tuple=True)
        return ma or 0, mi or 0

    def max_min_ask_till(self, time_delta=timedelta(seconds=10)):
        delta_time = datetime.utcnow() - time_delta
        ma, mi = self.get().filter(Venue.ask != 0,
                                   Venue.quote_time > delta_time
                                   ).select(fn.max(Venue.ask), fn.min(Venue.ask)
                                            ).scalar(as_tuple=True)
        return ma or 0, mi or 0

    def max_min_ask_bid_till(self, time_delta=timedelta(seconds=10)):
        max_bid, min_bid = self.max_min_bid_till(time_delta)
        max_ask, min_ask = self.max_min_ask_till(time_delta)
        return max_bid, min_bid, max_ask, min_ask

    def last_mm_bid(self):
        try:
            v = self.get().select(Venue.bid, Venue.quote_time
                                  ).filter(Venue.market_maker == True
                                           ).order_by(Venue.quote_time.desc()
                                                      ).limit(1).get()
            return v.bid, v.quote_time
        except DoesNotExist:
            return None, None

    def last_bid(self):
        try:
            v = self.get().select(Venue.bid, Venue.quote_time
                                  ).filter(Venue.bid != 0,
                                           ).order_by(Venue.quote_time.desc()
                                                      ).limit(1).get()
            return v.bid, v.quote_time
        except DoesNotExist:
            return None, None

    def last_mm_ask(self):
        try:
            v = self.get().select(Venue.ask, Venue.quote_time
                                  ).filter(Venue.market_maker == True
                                           ).order_by(Venue.quote_time.desc()
                                                      ).limit(1).get()
            return v.ask, v.quote_time
        except DoesNotExist:
            return None, None

    def last_ask(self):
        try:
            v = self.get().select(Venue.ask, Venue.quote_time
                                  ).filter(Venue.ask != 0
                                           ).order_by(Venue.quote_time.desc()
                                                      ).limit(1).get()
            return v.ask, v.quote_time
        except DoesNotExist:
            return None, None

    def spread_max_min_avg(self, time_delta=timedelta(seconds=10)):
        delta_time = datetime.utcnow() - time_delta

        ma, mi, av = Venue.raw(
                'SELECT MAX("t1"."ask" - "t1"."bid"), MIN("t1"."ask" - "t1"."bid"), AVG("t1"."ask" - "t1"."bid") '
                'FROM "venue" AS t1 '
                'WHERE (("t1"."market_maker" = ?) '
                'AND ("t1"."symbol" = ?) '
                'AND ("t1"."venue" = ?) '
                'AND ("t1"."quote_time" > ?)) ',
                True, self.symbol, self.venue, delta_time).scalar(as_tuple=True)
        return ma or 0, mi or 0, av or 0


class Transactions(BaseModel):
    tid = IntegerField(index=True)
    account = TextField(index=True)
    venue = TextField(index=True)
    symbol = TextField(index=True)
    price = IntegerField(index=True)
    size = IntegerField(index=True)
    time = DateTimeField(index=True)
    direction = TextField(index=True, choices=[(d.value, d.name) for d in Direction])
    filled_size = IntegerField(index=True, null=True)
    filled_price = IntegerField(index=True, null=True)
    filled_at = DateTimeField(index=True, null=True)
    open = BooleanField(index=True)

    class Meta:
        primary_key = CompositeKey('tid', 'account', 'venue', 'symbol')


if not Transactions.table_exists():
    Transactions.create_table()


class TransactionsUtil:
    def __init__(self, account, symbol, venue):
        self.account = account
        self.symbol = symbol
        self.venue = venue

    def get(self):
        return Transactions.filter(
                Transactions.account == self.account,
                Transactions.symbol == self.symbol,
                Transactions.venue == self.venue)

    @staticmethod
    def record(b_id, account, venue, symbol, price, size, time, direction=Direction.buy, filled_size=0,
               filled_price=0):
        logger.info('{} Transaction {}: {}:{}:{}->$:{}/{}, size:{}/{}, t:{}'.format(
                direction,
                b_id,
                account,
                venue,
                symbol,
                filled_price,
                price,
                filled_size,
                size,
                time
        ))
        Transactions.create(tid=b_id,
                            account=account,
                            venue=venue,
                            symbol=symbol,
                            price=price,
                            size=size,
                            time=time,
                            direction=direction.value,
                            filled_size=filled_size,
                            filled_price=filled_price,
                            open=True)

    @staticmethod
    def update_filled(account, venue, symbol, b_id, filled_size, filled_price, filled_at, is_open):
        logger.info('Update Transaction {}:{}:{}:{}->fs:{}, fp:{}, t:{}'.format(
                b_id,
                account,
                venue,
                symbol,
                filled_size,
                filled_price,
                filled_at
        ))
        count = Transactions.update(
                filled_size=filled_size,
                filled_price=filled_price,
                filled_at=filled_at,
                open=is_open
        ).where(
                Transactions.tid == b_id,
                Transactions.account == account,
                Transactions.venue == venue,
                Transactions.symbol == symbol
        ).execute()
        assert count == 1, 'Should have updated only one id'

    def total_bought_qty(self):
        s, p = self.get().filter(Transactions.open == False,
                                 Transactions.direction == Direction.buy.value
                                 ).select(fn.sum(Transactions.filled_size),
                                          fn.sum(Transactions.filled_price)).scalar(as_tuple=True)
        return s or 0, p or 0

    def total_sold_qty(self):
        s, p = self.get().filter(Transactions.open == False,
                                 Transactions.direction == Direction.sell.value
                                 ).select(fn.sum(Transactions.filled_size),
                                          fn.sum(Transactions.filled_price)).scalar(as_tuple=True)
        return s or 0, p or 0

    def total_bid(self):
        s, p = self.get().filter(Transactions.open == True,
                                 Transactions.direction == Direction.buy.value
                                 ).select(fn.sum(Transactions.size),
                                          fn.sum(Transactions.price)).scalar(as_tuple=True)
        return s or 0, p or 0

    def total_ask(self):
        s, p = self.get().filter(Transactions.open == True,
                                 Transactions.direction == Direction.sell.value
                                 ).select(fn.sum(Transactions.size),
                                          fn.sum(Transactions.price)).scalar(as_tuple=True)
        return s or 0, p or 0

    def bids_older_than(self, date_time):
        return self.get().filter(Transactions.open == True,
                                 Transactions.direction == Direction.buy.value,
                                 Transactions.time < date_time)

    def all_open_transactions(self):
        return self.get().select().filter(Transactions.open == True)

    def total_bid_price(self):
        return Transactions.raw(
                'SELECT (SUM("t1"."filled_price" * "t1"."filled_size") / SUM("t1"."filled_size")) '
                'FROM "transactions" AS t1 '
                'WHERE (("t1"."account" = ?) '
                'AND ("t1"."venue" = ?) '
                'AND ("t1"."symbol" = ?) '
                'AND ("t1"."direction" = ?)) ',
                self.account, self.venue, self.symbol, 'buy').scalar() or 0
