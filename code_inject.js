var sfPlotter = function() {
  var idGen = 1;
  $('head').append('<style type="text/css">' +
    '.x.axis line {' +
    'shape-rendering: auto;' +
    '} ' +
    '.line {' +
    'fill: none;' +
    'stroke: #000;' +
    'stroke-width: 1.5px;' +
    '}' +
    '</style>');

  function renderGraph(ws, cb) {
    "use strict";

    idGen = idGen + 1;
    var chartId = 'd3chart' + idGen;

    $('#page-wrapper').find('> div:last-child').before('<div class="row" id="' + chartId + '"></div>');

    var margin = {top: 6, right: 0, bottom: 6, left: 40},
      width = 960 - margin.right,
      height = 120 - margin.top - margin.bottom;

    var n = width / 2;

    var data = [0];

    var x = d3.scale.linear()
      .domain([0, n - 1])
      .range([0, width]);

    var y = d3.scale.linear()
      .domain([0, 10000])
      .range([height, 0]);

    var line = d3.svg.line()
      .interpolate("linear")
      .x(function(d, i) { return x(i); })
      .y(function(d, i) { return y(d); });

    var svg = d3.select('#' + chartId).append("p").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .style("margin-bottom", margin.left + "px")
      .append("g")
      .attr("transform", "translate(" + (margin.left + 30) + "," + margin.top + ")");

    var yAxis = d3.svg.axis().scale(y).ticks(5).orient("left");

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

    var path = svg.append("g")
      .append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line);

    var firstTick = true;

    var tick = function(newData) {

      if (newData !== parseInt(newData, 10)) {
        return;
      }
      // push a new data point onto the back
      data.push(newData);

      // redraw the line, and then slide it to the left
      path.attr("d", line);

      if (data.length >= n) {
        data.shift();
      }

      if (firstTick) {
        data.shift();
        firstTick = false;
      }


      y.domain([d3.min(data), d3.max(data)]);
      svg.selectAll("g.y.axis").call(yAxis);
    };


    var originalCallBack = ws.onmessage;
    var lastVal = 0;
    ws.onmessage = function() {
      var jEvent = JSON.parse(arguments[0].data);
      var eventVal = cb(jEvent);
      if (typeof(eventVal) === 'number' && eventVal > 1) {
        lastVal = eventVal;
        tick(eventVal);
      } else {
        tick(lastVal);
      }

      if (typeof(originalCallBack) === 'function') {
        return originalCallBack.apply(this, arguments);
      }
    };
  }

  // http://api.jquery.com/jQuery.when/
  $.when(
    $.getScript('//d3js.org/d3.v3.js')
  ).then(function(d3js, lodash) {
    console.log('And we have d3js');

    //{"ok":true,"quote":{"symbol":"ATI","venue":"EVOHEX","bid":6006,"bidSize":1676,"askSize":0,"bidDepth":3352,"askDepth":0,"last":6036,"lastSize":188,"lastTrade":"2015-12-22T06:34:07.08056019Z","quoteTime":"2015-12-22T06:34:07.440555079Z"}}
    var ws = _.values(window.webSocketAndPollerManager.websockets.quotes)[0];

    renderGraph(ws, function(jEvent) { return jEvent.quote.bid; });
    renderGraph(ws, function(jEvent) { return jEvent.quote.bidSize; });
    renderGraph(ws, function(jEvent) { return jEvent.quote.ask; });
    renderGraph(ws, function(jEvent) { return jEvent.quote.askSize; });
  }, function() {
    console.log('Oh! Something went wrong!!!');
  });
};
sfPlotter();