import argparse
import csv

import msgpack

from utils import get_logger, get_ns_timestamp

logger = get_logger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs='?', help="account id", required=True)
    parser.add_argument("--venue", nargs='?', help="venue name", required=True)
    parser.add_argument("--stock", nargs='?', help="venue name", required=True)
    args = parser.parse_args()

    csv_name = '{}.csv'.format(args.file)
    logger.info('Generating csv for file:{} with name:{}, venue:{}, stock:{}'.format(args.file, csv_name, args.venue, args.stock))

    def extract_fields(msg):
        if not msg.get('ok', False):
            logger.error('Not ok data: {}'.format(msg))

        quote = msg[b'quote']
        if not quote[b'symbol'] != args.stock:
            logger.error('Missing Stock: {}'.format(msg))

        if not quote[b'venue'] != args.venue:
            logger.error('Missing venue: {}'.format(msg))

        out = {'timestamp': quote[b'quoteTime'].decode('UTF-8')[:23]}

        def set_w(k, w):
            if k in quote:
                out[w] = quote[k]

        ask = quote.get(b'ask')
        bid = quote.get(b'bid')

        if ask and bid:
            set_w(b'bid', 'mm_bid')
            set_w(b'bidSize', 'mm_bid_size')
            set_w(b'ask', 'mm_ask')
            set_w(b'askSize', 'mm_ask_size')
        else:
            set_w(b'bid', 'bid')
            set_w(b'bidSize', 'bid_size')
            set_w(b'ask', 'ask')
            set_w(b'askSize', 'ask_size')
        return out

    fieldnames = ['timestamp', 'bid', 'ask', 'bid_size', 'ask_size', 'mm_bid', 'mm_ask', 'mm_bid_size', 'mm_ask_size']
    with open(args.file, 'rb') as data, open(csv_name, 'w') as csv_file:
        unpacker = msgpack.Unpacker(data)
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csv_writer.writeheader()
        for m in unpacker:
            csv_writer.writerow(extract_fields(m))


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logger.exception('Exception: {}'.format(e))
