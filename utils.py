import re
from datetime import datetime, timezone
import logging
import logging.config

# Example: 2016-01-05T15:17:21.969683087Z
date_re = re.compile(r'(?P<date>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{1,6})\d*Z')


def get_date(date_str):
    matched = date_re.match(date_str)
    if not matched:
        raise RuntimeError('Unknown Date format: {}'.format(date_str))
    return datetime.strptime(matched.group('date'), '%Y-%m-%dT%H:%M:%S.%f')


def get_ns_timestamp(date_str):
    parse = date_re.fullmatch(date_str)
    dt = datetime.strptime(parse.group('date'), '%Y-%m-%dT%H:%M')
    timestamp = dt.replace(tzinfo=timezone.utc).timestamp()
    timestamp_ns = timestamp * (10**9)
    seconds_ns = float(parse.group('seconds')) * (10**9)
    total_timestamp_ns = int(timestamp_ns + seconds_ns)
    return total_timestamp_ns


def get_total_fills_qty(fills):
    total_price = 0
    total_qty = 0
    for fill in fills:
        total_price += fill['price']
        total_qty += fill['qty']
    return total_price, total_qty


def is_near_max(minv, maxv, val, percent=20):
    """
    :param minv:
    :param maxv:
    :param val:
    :param percent:
    :return:

    tests if value is near min
    >>> is_near_max(1000, 7000, 2000)
    False
    >>> is_near_max(1000, 7000, 200)
    False
    >>> is_near_max(1000, 7000, -200)
    False
    >>> is_near_max(1000, 7000, 2200)
    False
    >>> is_near_max(1000, 7000, 8000)
    True
    >>> is_near_max(1000, 7000, 6000)
    True
    """
    val_percent = (val - minv)/(maxv - minv) * 100
    return val_percent > (100 - percent)


def is_near_min(minv, maxv, val, percent=20):
    """
    :param minv:
    :param maxv:
    :param val:
    :param percent:
    :return:

    tests if value is near min
    >>> is_near_min(1000, 7000, 2000)
    True
    >>> is_near_min(1000, 7000, 200)
    True
    >>> is_near_min(1000, 7000, -200)
    True
    >>> is_near_min(1000, 7000, 2200)
    False
    >>> is_near_min(1000, 7000, 8000)
    False
    >>> is_near_min(1000, 7000, 6000)
    False
    >>> is_near_min(-1000, 7000, -10)
    True
    >>> is_near_min(-1000, 7000, 10)
    True
    """
    val_percent = (val - minv)/(maxv - minv) * 100
    return val_percent < percent


def get_logger(name):
    if not hasattr(get_logger, 'config'):
        import os.path
        module_path = os.path.dirname(os.path.realpath(__file__))

        get_logger.config = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'verbose': {
                    'format': '%(levelname)s %(asctime)s %(process)d %(thread)d %(filename)s %(module)s %(funcName)s %(lineno)d %(message)s'
                },
                'simple': {
                    'format': '%(levelname)s %(message)s'
                },
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'verbose'
                },
                'file': {
                    'level': 'DEBUG',
                    'class': 'logging.handlers.TimedRotatingFileHandler',
                    'filename': os.path.join(module_path, 'logs/sf.log'),
                    'formatter': 'verbose',
                    'when': 'midnight',
                    'backupCount': 60,
                },
            },
            'root': {
                'handlers': ['file'],
                'level': 'DEBUG',
            }
        }
        logging.config.dictConfig(get_logger.config)

    return logging.getLogger(name)
