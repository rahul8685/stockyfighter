from http.cookies import SimpleCookie, Morsel

import pickle
from aiohttp.hdrs import COOKIE, SET_COOKIE

from sf_database import KeyValue


def handle_response_cookie(response):
    res_cookie_obj = SimpleCookie()
    for c in response.headers.getall(SET_COOKIE):
        res_cookie_obj.load(c)

    req_cookie_dict = {m.key: m.value for _, m in res_cookie_obj.items()}
    KeyValue.set_key('web_req_cookie', req_cookie_dict)


def get_req_cookies():
    return KeyValue.get_key('web_req_cookie', {})
