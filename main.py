#!/usr/bin/env python
import asyncio
import signal
import ujson as json

from datetime import timedelta

from client import SFClient, OrderType, Direction
from sf_database import VenueUtil, TransactionsUtil
from utils import get_date, get_logger

signal.signal(signal.SIGINT, signal.SIG_DFL)
logger = get_logger(__name__)


def main():
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    sfo = SFClient('e82f85eb62774d79315eab4628bf09ab44e144ce', loop)

    account_id = 'LLB84459375'
    venue_id = 'SKIHEX'
    symbol_id = 'OOVH'

    venue = VenueUtil(symbol_id, venue_id)
    transactions = TransactionsUtil(account_id, symbol_id, venue_id)

    look_back_sec = 300

    async def stop():
        await sfo.close()
        loop.stop()

    async def sell_side():
        try:
            sleep_time = 0.5

            new_bid_size = 10
            new_ask_size = 10
            max_bid_short = 10
            effective_price_percent = 12.5 / 100
            effective_region_percent = 30 / 100
            max_time_difference = 4
            minimum_spread = 10

            def effective_bid_ask_price():
                try:
                    last_ask_price, ask_time = venue.last_ask()
                    last_bid_price, bid_time = venue.last_bid()
                    max_spread, min_spread, average_spread = venue.spread_max_min_avg(timedelta(seconds=look_back_sec))
                    nothing = None, None, None, None, None, None

                    if not all((last_ask_price, last_bid_price, ask_time, bid_time, average_spread, max_spread,
                                min_spread,)):
                        return nothing

                    last_spread = last_ask_price - last_bid_price

                    def should_select_one():
                        if ask_time == bid_time:
                            return False

                        if last_ask_price < last_bid_price:
                            return True

                        time_difference = (ask_time - bid_time).total_seconds()

                        s_o = False
                        if time_difference > max_time_difference:
                            s_o = True

                        if last_spread > max_spread or last_spread < min_spread:
                            s_o = True

                        return s_o

                    select_one = should_select_one()

                    if select_one:
                        if ask_time > bid_time:
                            selected_ask_price = last_ask_price
                            selected_bid_price = selected_ask_price - average_spread
                        else:
                            selected_bid_price = last_bid_price
                            selected_ask_price = selected_bid_price + average_spread
                    else:
                        selected_bid_price = last_bid_price
                        selected_ask_price = last_ask_price

                    selected_spread = selected_ask_price - selected_bid_price

                    if selected_spread <= minimum_spread:
                        return nothing

                    spread_diff = effective_price_percent * selected_spread
                    region_diff = effective_region_percent * selected_spread

                    e_bid_price = selected_bid_price + spread_diff
                    e_ask_price = selected_ask_price - spread_diff

                    max_bid = selected_bid_price + region_diff
                    min_bid = selected_bid_price

                    max_ask = selected_ask_price
                    min_ask = selected_ask_price - region_diff

                    return e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask
                except Exception as e:
                    logger.exception(e)
                    stop()

            async def monitor_bids():
                try:
                    while True:
                        await asyncio.sleep(sleep_time)

                        e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask = effective_bid_ask_price()

                        if not all((e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask,)):
                            continue

                        open_transactions = transactions.all_open_transactions()
                        for trans in open_transactions:
                            if trans.direction == Direction.buy:
                                if trans.price < min_bid or trans.price > max_bid:
                                    await sfo.order_cancel(venue_id, symbol_id, trans.tid)
                            else:
                                if trans.price < min_ask or trans.price > max_ask:
                                    await sfo.order_cancel(venue_id, symbol_id, trans.tid)
                except Exception as e:
                    logger.exception(e)
                    stop()

            async def buyer():
                try:
                    while True:
                        await asyncio.sleep(sleep_time)

                        total_bid_qty, tot_bid_price = transactions.total_bought_qty()
                        total_ask_qty, tot_ask_price = transactions.total_sold_qty()
                        if (total_bid_qty - total_ask_qty) > max_bid_short:
                            continue

                        e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask = effective_bid_ask_price()
                        if not e_bid_price:
                            continue

                        await sfo.new_order(account_id,
                                            venue_id,
                                            symbol_id,
                                            e_bid_price,
                                            new_bid_size,
                                            Direction.buy,
                                            OrderType.limit)
                except Exception as e:
                    logger.exception(e)
                    stop()

            async def seller():
                try:
                    while True:
                        await asyncio.sleep(sleep_time)

                        total_bid_qty, tot_bid_price = transactions.total_bought_qty()
                        total_ask_qty, tot_ask_price = transactions.total_sold_qty()

                        total_available_stock_qty = total_bid_qty - total_ask_qty
                        if total_available_stock_qty <= 0:
                            continue

                        current_stock_price = transactions.total_bid_price()
                        if not current_stock_price:
                            continue

                        e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask = effective_bid_ask_price()

                        if not all((e_bid_price, e_ask_price, max_bid, min_bid, max_ask, min_ask,)):
                            continue

                        if e_ask_price < current_stock_price:
                            continue

                        size = new_ask_size
                        if size > total_available_stock_qty:
                            size = total_available_stock_qty

                        await sfo.new_order(account_id,
                                            venue_id,
                                            symbol_id,
                                            e_ask_price,
                                            size,
                                            Direction.sell,
                                            OrderType.limit)
                except Exception as e:
                    logger.exception(e)
                    stop()

            await asyncio.wait([
                monitor_bids(),
                buyer(),
                seller(),
            ])
        except Exception as e:
            logger.exception(e)
            stop()

    async def setup():
        try:
            async def record_venue_quotes(message):
                try:
                    json_msg = json.loads(message)
                    logger.info('Quote: {}'.format(json_msg))
                    if not json_msg['ok']:
                        print('Something Wrong in {}'.format(message))
                        return False

                    quote = json_msg['quote']
                    quote_time = get_date(quote['quoteTime'])
                    venue.record(symbol=symbol_id,
                                 venue=venue_id,
                                 bid=quote.get('bid', 0),
                                 ask=quote.get('ask', 0),
                                 bid_size=quote.get('bidSize', 0),
                                 ask_size=quote.get('askSize', 0),
                                 bid_depth=quote.get('bidDepth', 0),
                                 ask_depth=quote.get('askDepth', 0),
                                 quote_time=quote_time)

                    return True
                except Exception as e:
                    logger.exception(e)
                    stop()

            async def record_executions(ex):
                try:
                    json_ex = json.loads(ex)

                    order = json_ex['order']
                    fills = order['fills']

                    if not order['ok']:
                        logger.error('Not OK execution: {}'.format(json_ex))
                        return

                    total_price = 0
                    total_qty = 0
                    for fill in fills:
                        total_price += fill['price']
                        total_qty += fill['qty']

                    transactions.update_filled(order['account'],
                                               order['venue'],
                                               order['symbol'],
                                               order['id'],
                                               total_qty,
                                               total_price,
                                               get_date(json_ex['filledAt']),
                                               order['open'])
                except Exception as e:
                    logger.exception(e)
                    stop()

            async def monitor():
                try:
                    while True:
                        await asyncio.sleep(2)
                        total_bought_qty, tot_bought_price = transactions.total_bought_qty()
                        total_sold_qty, tot_sold_price = transactions.total_sold_qty()

                        total_bid_qty, tot_bid_price = transactions.total_bid()
                        total_ask_qty, tot_ask_price = transactions.total_ask()
                        current_stock_price = transactions.total_bid_price()

                        max_spread, min_spread, average_spread = venue.spread_max_min_avg(
                            timedelta(seconds=look_back_sec))

                        logger.info('total_bought_qty:{}  tot_bought_price:{}'.format(total_bought_qty,
                                                                                      tot_bought_price))
                        logger.info('total_sold_qty:{}  tot_sold_price:{}'.format(total_sold_qty,
                                                                                  tot_sold_price))

                        logger.info('total_bid_qty:{}  tot_bid_price:{}'.format(total_bid_qty,
                                                                                tot_bid_price))
                        logger.info('total_ask_qty:{}  tot_ask_price:{}'.format(total_ask_qty,
                                                                                tot_ask_price))

                        logger.info(
                            'cash:{}, max_spread:{}, min_spread:{}, average_spread:{}, current_stock_price:{}'.format(
                                tot_sold_price - tot_bought_price,
                                max_spread,
                                min_spread,
                                average_spread,
                                current_stock_price))

                except Exception as e:
                    logger.exception(e)
                    stop()

            await asyncio.wait([
                sfo.monitor_venue(account_id, venue_id, record_venue_quotes),
                sfo.monitor_executions(account_id, venue_id, record_executions),
                monitor()
            ])
        except Exception as e:
            logger.exception(e)
            stop()

    async def start():
        try:
            await asyncio.wait([
                setup(),
                sell_side()
            ])
        except Exception as e:
            logger.exception(e)
            stop()

    loop.run_until_complete(start())


if __name__ == "__main__":
    main()
