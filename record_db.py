import argparse
import json

import msgpack

from utils import get_logger

logger = get_logger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs='?', help="account id", required=True)
    parser.add_argument("--venue", nargs='?', help="venue name", required=True)
    parser.add_argument("--stock", nargs='?', help="venue name", required=True)
    args = parser.parse_args()

    json_name = '{}.json'.format(args.file)
    logger.info('Recording DB for file:{} with name:{}, venue:{}, stock:{}'.format(args.file, json_name, args.venue, args.stock))

    counter = 1

    def extract_fields(msg):
        nonlocal counter
        if not msg.get('ok', False):
            logger.error('Not ok data: {}'.format(msg))

        quote = msg[b'quote']
        if not quote[b'symbol'] != args.stock:
            logger.error('Missing Stock: {}'.format(msg))

        if not quote[b'venue'] != args.venue:
            logger.error('Missing venue: {}'.format(msg))

        counter += 1
        out = {'timestamp': counter}

        def set_w(k, w):
            if k in quote:
                out[w] = quote[k]

        ask = quote.get(b'ask')
        bid = quote.get(b'bid')

        if ask and bid:
            set_w(b'bid', 'mm_bid')
            set_w(b'bidSize', 'mm_bid_size')
            set_w(b'ask', 'mm_ask')
            set_w(b'askSize', 'mm_ask_size')
        else:
            set_w(b'bid', 'bid')
            set_w(b'bidSize', 'bid_size')
            set_w(b'ask', 'ask')
            set_w(b'askSize', 'ask_size')
        return out

    with open(args.file, 'rb') as data, open(json_name, 'w') as json_file:
        unpacker = msgpack.Unpacker(data)
        json_file.write('[')
        for m in unpacker:
            json_file.write(json.dumps(extract_fields(m), indent=4))
            json_file.write(',')
        json_file.write(']')


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        logger.exception('Exception: {}'.format(e))
