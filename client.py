import json
from enum import Enum
from urllib.parse import urlencode

import aiohttp
import websockets
from aiohttp.hdrs import CONTENT_TYPE, COOKIE, SET_COOKIE, METH_GET, METH_POST, HOST, REFERER, ORIGIN, USER_AGENT, \
    ACCEPT, ACCEPT_ENCODING, ACCEPT_LANGUAGE, METH_DELETE

from cookie_handler import handle_response_cookie, get_req_cookies
from sf_database import Direction, TransactionsUtil
from utils import get_date, get_total_fills_qty, get_logger

logger = get_logger(__name__)


class OrderType(Enum):
    limit = 'limit'
    market = 'market'
    fill_or_kill = 'fill-or-kill'
    immediate_or_cancel = 'immediate-or-cancel'


class SFClient:
    api_base = 'https://api.stockfighter.io/ob/api'
    ws_api_base = 'wss://www.stockfighter.io/ob/api/ws'

    def __init__(self, k, l):
        self.api_key = k
        self.loop = l
        self.session = aiohttp.ClientSession(loop=self.loop,
                                             headers={'X-Starfighter-Authorization': self.api_key})
        self.websockets_set = set()

    async def close(self):
        self.session.close()
        await self.close_all_websocket()

    @staticmethod
    def check_ok_status(j):
        if isinstance(j, dict):
            ok = j.get('ok', True)
            if not ok:
                raise Exception('Error: {}'.format(j))
        return j

    async def http_req(self, complete_path, do_post=False, post_dict=None, do_delete=False):
        req_attr = {'url': complete_path, 'method': METH_GET}
        if do_post:
            req_attr['method'] = METH_POST
        elif do_delete:
            req_attr['method'] = METH_DELETE
        if post_dict:
            req_attr['data'] = json.dumps(post_dict)

        response = await self.session.request(**req_attr)
        if response.status != 200:
            response_text = await response.text()
            raise RuntimeError('Failed in Request: {} with response: {}'.format(complete_path, response_text))

        if SET_COOKIE in response.headers:
            handle_response_cookie(response)

        is_json = any('application/json' in s for s in response.headers.getall(CONTENT_TYPE))
        if is_json:
            return await response.json()
        else:
            return await response.text()

    async def get(self, path):
        r = await self.http_req('{}/{}'.format(SFClient.api_base, path))
        return self.check_ok_status(r)

    async def delete(self, path):
        r = await self.http_req('{}/{}'.format(SFClient.api_base, path), do_delete=True)
        return self.check_ok_status(r)

    async def post(self, path, post_dict=None):
        r = await self.http_req('{}/{}'.format(SFClient.api_base, path), do_post=True, post_dict=post_dict)
        return self.check_ok_status(r)

    async def api_heartbeat(self):
        return await self.get('heartbeat')

    async def venue_heartbeat(self, venue):
        return await self.get('venues/{}/heartbeat'.format(venue))

    async def venue_stock(self, venue):
        return await self.get('venues/{}/stocks'.format(venue))

    async def stock_order_book(self, venue, stock):
        return await self.get('venues/{}/stocks/{}'.format(venue, stock))

    async def stock_quote(self, venue, stock):
        return await self.get('venues/{}/stocks/{}/quote'.format(venue, stock))

    async def order_status(self, venue, stock, order_id):
        return await self.get('venues/{}/stocks/{}/orders/{}'.format(venue, stock, order_id))

    async def order_cancel(self, venue, stock, order_id):
        response = await self.delete('venues/{}/stocks/{}/orders/{}'.format(venue, stock, order_id))
        total_price, total_qty = get_total_fills_qty(response['fills'])
        TransactionsUtil.update_filled(response['account'],
                                       response['venue'],
                                       response['symbol'],
                                       response['id'],
                                       total_qty,
                                       total_price,
                                       get_date(response['ts']),
                                       response['open'])
        return response

    async def new_order(self, account, venue, stock, price, qty, buy=Direction.buy, order_type=OrderType.fill_or_kill):
        post_dict = {
            'account': account,
            'venue': venue,
            'stock': stock,
            'price': round(price),
            'qty': qty,
            'direction': buy.value,
            'orderType': order_type.value
        }

        if order_type == OrderType.market:
            del post_dict['price']

        response = await self.post('venues/{}/stocks/{}/orders'.format(venue, stock), post_dict)

        TransactionsUtil.record(response['id'],
                                account,
                                venue,
                                stock,
                                price,
                                qty,
                                get_date(response['ts']),
                                buy)
        return response

    async def get_websocket(self, address):
        new_websocket = await websockets.connect(address)
        self.websockets_set.add(new_websocket)
        return new_websocket

    async def close_websocket(self, websocket):
        if websocket.open:
            await websocket.close()
        self.websockets_set.remove(websocket)

    async def close_all_websocket(self):
        for websocket in self.websockets_set:
            if websocket.open:
                await websocket.close()

        self.websockets_set.clear()

    async def monitor_websocket(self, path, consumer):
        websocket = await self.get_websocket('{}/{}'.format(self.ws_api_base, path))
        while True:
            message = await websocket.recv()
            if message is None:
                break
            if not await consumer(message):
                break

    def monitor_venue(self, trading_account, venue, consumer):
        return self.monitor_websocket('{}/venues/{}/tickertape'.format(trading_account, venue), consumer)

    def monitor_executions(self, trading_account, venue, consumer):
        return self.monitor_websocket('{}/venues/{}/executions'.format(trading_account, venue), consumer)
