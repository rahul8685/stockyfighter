import argparse
import asyncio
import signal
from functools import partial
import msgpack
import json

from client import SFClient
from utils import get_logger


logger = get_logger(__name__)


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    parser = argparse.ArgumentParser()
    parser.add_argument("--account", nargs='?', help="account id", required=True)
    parser.add_argument("--venue", nargs='?', help="venue name", required=True)
    args = parser.parse_args()

    logger.info('Collecting from account:{}, venue:{}'.format(args.account, args.venue))

    file_name = 'data_{}_{}.mp'.format(args.account, args.venue)
    loop = asyncio.get_event_loop()
    packer = msgpack.Packer()

    file = open(file_name, 'wb')
    async def collect_venue_quotes(message):
        logger.info('New Message: {}'.format(message))
        json_msg = json.loads(message)
        file.write(packer.pack(json_msg))
        return True

    sfo = SFClient('e82f85eb62774d79315eab4628bf09ab44e144ce', loop)
    try:
        loop.run_until_complete(sfo.monitor_venue(args.account, args.venue, partial(collect_venue_quotes, file=file)))
    finally:
        file.close()
        sfo.close()

if __name__ == '__main__':
    main()
